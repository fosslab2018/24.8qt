from PyQt4 import QtGui
import sys
import diyak    
# This is the name of the converted  UI python file 
#Ui_Calculator is the name of the class, created in QtDesigner
class calcuApp(QtGui.QMainWindow, diyak.Ui_MainWindow):
	def __init__(self):
		super(self.__class__,self).__init__()
		self.setupUi(self)
		self.pushButton.clicked.connect(self.add)
# pBsum is the name of the button in the UI. 
# When its clicked the add function will be called
	def add(self):
		sumV = int(self.lineEdit.text()) + int(self.lineEdit_2.text())# IE1.text() gives the value inthe lineText named IE1 and so on...
		self.lineEdit_3.setText(str(sumV))# IEres.setText set the value of the varable sumV in lineText named  IEres
def main():
	app = QtGui.QApplication(sys.argv)  # A new instance of QApplication
	form = calcuApp()  # Name of the class created above: here calcuAPP
	form.show()  # Show the form
	app.exec_()  # and execute the app
if __name__ == '__main__':  # if we're running file directly and not importing it
	main()  # run the main function
